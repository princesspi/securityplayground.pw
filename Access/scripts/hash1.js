function stringify(raw) {
	return CryptoJS.enc.Latin1.stringify(raw);
}

function hash1(passphrase) {

    var salt = CryptoJS.SHA3(passphrase, { outputLength: 256 });
    var key = CryptoJS.PBKDF2(passphrase, salt, { keySize: 256/32, iterations: 1024  });
	
	return stringify(key);
}
